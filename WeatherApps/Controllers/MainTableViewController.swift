//
//  MainTableViewController.swift
//  WeatherApps
//
//  Created by Konstantin Pischanskyi on 06.11.2019.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {
    
    var dailyArray = [DailyWeatherModel]()

    
    @IBOutlet weak var tempLabelToday: UILabel!
    @IBOutlet weak var pressureLabelToday: UILabel!
    @IBOutlet weak var humidityLabelToday: UILabel!
    @IBOutlet weak var timezoneLabel: UILabel!
    @IBOutlet weak var PullLabel: UILabel!
    
    
    //MARK: - Variable
    
    var track = LocationNetworkService()
    
    var myRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Updating data")
        refreshControl.tintColor = .white
        return refreshControl
    }()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = UIImageView(image: UIImage(named: "default"))
        track.startLocation()
        tableView.refreshControl = myRefreshControl
        
    }

    
    //MARK: - Function

    @objc private func refresh(_ sender: UIRefreshControl) {
        self.PullLabel.isHidden = true
         track.startLocation()
         updateViewUI()
         sender.endRefreshing()
     }
    
    
    private func updateViewUI(){
        WeatherNetworkService.getWeather { (weather) in
            print(weather.timezone)
            self.tempLabelToday.text = weather.currently.temperatureString
            self.pressureLabelToday.text = weather.currently.pressureString
            self.humidityLabelToday.text = weather.currently.humidityString
            self.timezoneLabel.text = weather.timezone
            
            self.tableView.backgroundView = UIImageView(image: UIImage(named: weather.currently.iconForBack))

            print(weather.currently.iconForBack)
            
            print(self.dailyArray)
            
            self.dailyArray.removeAll()
            for value in weather.daily.data {
                self.dailyArray.append(DailyWeatherModel(day: value.time, tempMax: value.temperatureMax, tempMin: value.temperatureMin, icon: value.icon))
            }
            print(self.dailyArray)
            self.dailyArray.remove(at: 0)
            self.dailyArray.removeLast()
            self.tableView.reloadData()
        }
    }
    
    
    // MARK: - Table view data source


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dailyArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DailyWeatherTableViewCell
        
        cell.backgroundColor = .clear
        cell.iconView.image = UIImage(named: dailyArray[indexPath.row].icon)
        cell.tempLabel.text = dailyArray[indexPath.row].tempMaxStr + "/" + dailyArray[indexPath.row].tempMinStr + "˚C"
        cell.timezoneLabel.text = dailyArray[indexPath.row].dayStr
        return cell
    }
   


}
