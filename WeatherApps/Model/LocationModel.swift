//
//  LocationModel.swift
//  WeatherApps
//
//  Created by Konstantin Pischanskyi on 04.11.2019.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//



import Foundation

struct Coordinates {
    let latitude: Double
    let longitude: Double
}
