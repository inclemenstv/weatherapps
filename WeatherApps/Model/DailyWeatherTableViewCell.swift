//
//  DailyWeatherTableViewCell.swift
//  WeatherApps
//
//  Created by Konstantin Pischanskyi on 06.11.2019.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import UIKit

class DailyWeatherTableViewCell: UITableViewCell {

    
    @IBOutlet weak var timezoneLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var iconView: UIImageView!
    
    
    
}

