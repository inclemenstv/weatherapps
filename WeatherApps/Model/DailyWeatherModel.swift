//
//  DailyWeatherModel.swift
//  WeatherApps
//
//  Created by Konstantin Pischanskyi on 06.11.2019.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import Foundation

struct DailyWeatherModel {
    
    var day: Double
    var tempMax: Double
    var tempMin: Double
    var icon: String
    
}

extension DailyWeatherModel {
    
    var dayStr: String {
        let date = Date(timeIntervalSince1970: day)
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "EEEE"
        return dateFormatterPrint.string(from: date)
    }
    
    var tempMaxStr: String {
        return "\(Int(tempMax - 32))"
    }
    
    var tempMinStr: String {
        return "\(Int(tempMin - 32))"

    }
    
}
