//
//  WeatherModel.swift
//  WeatherApps
//
//  Created by Konstantin Pischanskyi on 04.11.2019.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import Foundation




struct CurrentWeather: Codable {
    var currently: Currently
    var daily: Daily
    var timezone: String
}



struct Currently: Codable {
    var time: Double
    var temperature: Double
    var pressure: Double
    var humidity: Double
    var icon: String
    
}

struct Daily: Codable {
    var data: [Data]
}

struct Data: Codable {
    var time: Double
    var temperatureMax: Double
    var temperatureMin: Double
    var icon: String
}


extension Currently {
    
    var temperatureString: String {
        return "\(Int(temperature - 32))˚C"
    }
    var pressureString: String {
    return "\(Int(pressure)) mm"
    }
    
    var humidityString: String {
            return "\(Int(humidity)) %"
        }
    
    var iconForBack: String {
        return "\(icon)Back"
    }
    
}




//MARK: - Model for OpenWeatherMap

//struct CurrentWeather: Codable {
//    let main: Weather
//    let coord: Coord
//    let name : String
//    let weather: [WeatherDetails]
//}
//
//struct Weather: Codable {
//    let temp: Double
//    let humidity: Double
//    let pressure: Double
//
//}
//
//
//struct Coord: Codable {
//    let lon: Double
//    let lat: Double
//}
//
//struct WeatherDetails: Codable {
//    let id: Int
//    let main: String
//    let description: String
//    let icon: String
//}
//
//
//extension Weather {
//    var pressureString: String {
//        return "\(Int(pressure)) mm"
//    }
//
//    var humidityString: String {
//        return "\(Int(humidity)) %"
//    }
//
//    var temperatureString: String {
//        return "\(Int(temp - 273.15))˚C"
//    }
//}
