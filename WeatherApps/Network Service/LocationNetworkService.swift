//
//  LocationNetworkService.swift
//  WeatherApps
//
//  Created by Konstantin Pischanskyi on 04.11.2019.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

//
//  MyLocation.swift
//  MVC Weather
//
//  Created by Konstantin Pischanskyi on 6/27/19.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import CoreLocation




class LocationNetworkService: NSObject, CLLocationManagerDelegate {
    
    var locationManager = CLLocationManager()

    
    func startLocation () {
        
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            
        }
    
    
}

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if let location = locations.first {
            print("latitude: \(location.coordinate.latitude) and longitude \(location.coordinate.longitude)")
            let coordinates = Coordinates(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            coordinat = coordinates
            print(coordinat)
            
        }
}
}

