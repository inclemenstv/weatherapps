//
//  WeatherNetwork.swift
//  WeatherApps
//
//  Created by Konstantin Pischanskyi on 04.11.2019.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import Foundation
import CoreLocation





//MARK: Variable:

//let locationManager = CLLocationManager()
var coordinat = Coordinates(latitude: 0, longitude: 0)
let appiKey = "b3e0aad876fa893c9ba75f4ede04a548/"
let base = "https://api.darksky.net/forecast/"


class WeatherNetworkService {

    private init() {}
    


    static func getWeather(completion: @escaping(CurrentWeather) -> () ) {
        
        

        guard let url = URL(string: "\(base)\(appiKey)\(coordinat.latitude),\(coordinat.longitude)") else { return }

        NetworkService.shared.getData(url: url) { (json) in
            do {
                DispatchQueue.main.async {
                    completion(json as! CurrentWeather)
                }
            }
            }
    }
}
