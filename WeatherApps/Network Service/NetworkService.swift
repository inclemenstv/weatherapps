//
//  NetworkService.swift
//  WeatherApps
//
//  Created by Konstantin Pischanskyi on 04.11.2019.
//  Copyright © 2019 Konstantin Pischanskyi. All rights reserved.
//

import Foundation

class NetworkService {
    
    private init() {}
    
    static let shared = NetworkService()
    
    public func getData(url: URL, completion: @escaping (Any) -> () ) {
        let session = URLSession.shared
        
        session.dataTask(with: url) {(data, response, error) in
            guard let data = data else { return }
            print(data)
            do {
                let weather = try! JSONDecoder().decode(CurrentWeather.self, from: data)
                DispatchQueue.main.async {
                    completion(weather)
                }
            }
        }.resume()
    }
}

